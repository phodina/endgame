SH := /bin/sh
GREEN := \033[1;36m
CLEAR := \033[0m
HOSTNAME := $(shell hostname | sed 's/\(.*\)/\L\1/')
HOST_NAME := $(shell hostname | sed 's/\(.*\)/\U\1/')

#all: --config-${HOSTNAME} --config-pine --config-rpi --config-server
all: --config-${HOSTNAME} --config-home

## Config Targets - Tangle literate config into config files
## ==============================================
.ONESHELL:
--config-${HOSTNAME}: --config-laptop
	@{ \
		echo -e "${GREEN}Building ${HOST_NAME} ...${CLEAR}"
	emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "Laptop-${HOST_NAME}.org")'
	}

.ONESHELL:
--config-laptop:
	@{ \
		echo -e "${GREEN}Building Laptop ...${CLEAR}"
	emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "Laptop.org")'
	}

.ONESHELL:
--config-home: --config-build
	@{ \
		echo -e "${GREEN}Building Home ...${CLEAR}"
	emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "Home.org")'
	}

.ONESHELL:
--config-pine:
	@{ \
		echo -e "${GREEN}Building Pine ...${CLEAR}"
	emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "Pine.org")'
	}

.ONESHELL:
--config-rpi:
	@{ \
		echo -e "${GREEN}Building Raspberry Pi ...${CLEAR}"
	emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file
	"RaspberryPi.org")'
	}

.ONESHELL:
--config-server:
	@{ \
		echo -e "${GREEN}Building Server ...${CLEAR}"
	emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file
	"Server.org")'
	}

.ONESHELL:
--config-build:
	@{ \
		mkdir -p ./build/config/{alacritty,i3,i3status-rust,sway,ssh,screenlayout}
		mkdir -p ./guix
	}

## Deployment Targets - run guix system reconfigure to install new OS generation
.ONESHELL:
--deploy-${HOSTNAME}-system:
	@{ \
		echo -e "${GREEN}Deploying Guix System ...${CLEAR}"
	#cores=$(nproc)
	if sudo guix system --cores=4 -L ./build reconfigure ./build/laptop-${HOSTNAME}.scm; then
	echo -e "${GREEN} Finished deploying Guix System.${CLEAR}"
	fi
	}

.ONESHELL:
--deploy-home:
	@{ \
		echo -e "${GREEN}Deploying Guix Home ...${CLEAR}"
	#cores=$(nproc)
	if guix home --cores=4 -L ./build reconfigure ./build/home.scm; then
	echo -e "${GREEN} Finished deploying Guix Home.${CLEAR}"
	fi
	}

install: --deploy-${HOSTNAME}-system --deploy-home

clean:
	@echo "Removing build artifacts ..."
	@rm -rf build
