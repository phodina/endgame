#!/bin/sh

set -e

EFI_PARTITION=$1
GUIX_PARTITION=$2
SWAP_SIZE=$3
HOSTNAME=$4

if [ $# -ne 4 ];then
    echo "Usage: $0 <EFI_PARTITION> <GUIX_PARTITION> <SWAP_SIZE> <HOSTNAME>"
    echo "Example: $0 /dev/sdx1 /dev/sdx2 8G example"
    exit 1
fi

# To wipe out old partitions use e.g.
# sgdisk --zap-all <DRIVE>
# sgdisk --clear --new=1:0:+512MiB --typecode=1:ef00 --change-name=1:EFI
# --new=2:0:0 --typecode=2:8E00 --change-name=2:cryptsystem <DRIVE>

partition(){
   # Create and mount LUKS partition
    cryptsetup luksFormat --cipher=aes-xts-plain64 "${GUIX_PARTITION}"
    cryptsetup luksOpen "${GUIX_PARTITION}" cryptsystem

    # Crate BTRFS and FAT filesystems
    mkfs.btrfs -f /dev/mapper/cryptsystem
    mkfs.fat -F32 -n boot "${EFI_PARTITION}"

    # Wait for changes to sync to disc
    sleep 3
    sync

    # Mount BTRFS partition and create subvolumes
    mount -t btrfs /dev/mapper/cryptsystem /mnt
    btrfs subvolume create /mnt/root
    btrfs subvolume create /mnt/boot
    btrfs subvolume create /mnt/home
    btrfs subvolume create /mnt/gnu
    btrfs subvolume create /mnt/data
    btrfs subvolume create /mnt/log
    btrfs subvolume create /mnt/swap

    # Create swapfile
    touch /mnt/swap/swapfile
    truncate -s 0 /mnt/swap/swapfile
    chattr +C /mnt/swap/swapfile
    fallocate -l "${SWAP_SIZE}" /mnt/swap/swapfile
    chmod 600 /mnt/swap/swapfile
    btrfs property set /mnt/swap/swapfile compression none
    mkswap /mnt/swap/swapfile

    # Create snapshot of clean initial state
    # this will later be useful when implementing opt-in state in Guix System
    # https://mt-caret.github.io/blog/posts/2020-06-29-optin-state.html
    # TODO: Implement opt-in state on BTRFS
    btrfs subvolume snapshot -r /mnt/root /mnt/root-blank
    umount /mnt
}

mount_partitions (){
    mount -o subvol=root,compress=zstd,noatime /dev/mapper/cryptsystem /mnt

    mkdir /mnt/home
    mkdir /mnt/gnu
    mkdir /mnt/data
    mkdir -p /mnt/var/log
    mkdir /mnt/boot

    mount -o subvol=boot,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/boot
    mount -o subvol=home,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/home
    mount -o subvol=gnu,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/gnu
    mount -o subvol=data,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/data
    mount -o subvol=log,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/var/log

    mkdir /mnt/boot/efi
    mount "${EFI_PARTITION}" /mnt/boot/efi
}

guix_setup (){
    # Mount the /gnu/store in /mnt and pull
    herd start cow-store /mnt
    guix pull
}

emacs_org (){
    # Create environment with Emacs and ORG mode in order to generate
    # the machine config based on the HOSTNAME
    guix shell emacs emacs-org make -- make HOSTNAME=${HOSTNAME}
}

partition
mount_partitions
guix_setup
emacs_org

echo "Install script finished, environment setup"
echo "You can now run: guix system init"
