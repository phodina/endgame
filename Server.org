#+TITLE: Server
#+STARTUP: content
#+PROPERTY: header-args :mkdirp yes
#+PROPERTY: header-args:sh :tangle-mode (identity #o555)
#+PROPERTY: header-args:conf :tangle-mode (identity #o555)

* Table of Contents
:PROPERTIES:
:TOC :include all :ignore this :depth 5
:CONTENTS:
:END:

* Server
** Dependencies
#+NAME: base-definition
#+BEGIN_SRC scheme :tangle build/server.scm
(define-module (server)
  #:use-module (system)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-1)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system shadow)
  #:use-module (gnu system file-systems)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services ssh)
  #:use-module (gnu services docker)
  #:use-module (gnu services desktop)
  #:use-module (gnu services linux)
  #:use-module (gnu services networking)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services vpn))

#+END_SRC

*** SSH key

#+NAME: base-definition
#+BEGIN_SRC scheme :tangle build/server.scm
(define %cylon2p0-ssh-key
  (plain-file "id_rsa.pub"
              "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDg/3m4MUAmsXyDrFcCtAMmg90NZwRIKMhhlk59V+CxdQy3IrKi5Xp3bqXr1hm0Z7nAFdBKEenK5nXSeuwzf3On43QAZoRbsm0/8J/5BZ+4nfpQ0iCZaSsbDR1eb6Qvcxnth5EeGJDNEaMDDCriM0v6lnDHCzJpIpMKC1cZd+bE4nEBhKQPxKs+/1DFV76GuTG0IQqHrq2HTQmm03zcuH/tg7if5tMWbJW1FXrZNyvtHl8VwdpRo4n8bLiIK8x8mJvnm6USCxmbI/M7nBvX5jwEJ1BOSzBRs+r7LRZ4bGneaoWBSyKgTMFAB9xGp1q0BUx26k7Gh2Bocl1A14oJIjnQ3QRmz+ZMIjtfObLdLaC5WoP6N++ienDOLH4Uxc8AmaJaBso9jseeaieGh3+NnfpqZ5QCN5J024zal0YCkvdebib1Xpy5MTnDaU9TRIQmLyNfyzyu9oD7GmW2HRbCBKFUWFUw/pbHtzSTJmZoj2+uEAVXqXu2adPD5jG3SXWAdELEKOtFK/PsgqLUPR6Gg/WCBfdKdER4XYgDyo+23TZ7LnbkToQY1xh+aQ9eNcHd/ArM3VBUl7ONU2sn36qS+1ELCr65YDVKDkUQm8S+OR098/RqvnDHnj3MhRgr4WteId8f3CHckpj8JA+UTc3a5r95HYDhglknk+JvYR+8DiqVKw== cardno:000611077967"))
#+END_SRC

** Base definition
#+NAME: server-operating-system
#+BEGIN_SRC scheme :tangle build/server.scm
(operating-system
 (inherit base-operating-system)
 (host-name "were-wolf")

#+END_SRC

** File Systems
Werewolf machine uses the old BIOS MBR partition table

#+NAME: file-systems
#+BEGIN_SRC scheme :tangle build/server.scm
(bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (targets (list "/dev/sda"))))
(swap-devices
    (list (uuid "91c246d3-7e9b-4c84-bab0-fd76e7e57490")))

  (file-systems
    (cons*
      (file-system
        (mount-point "/")
        (device (uuid "d7bce5e9-0ec1-4093-9b33-602597e528a5" 'ext4))
        (type "ext4"))
      %base-file-systems))
#+END_SRC

** Users
Setup the one user, me, and establish a core set of groups.

#+NAME: users
#+BEGIN_SRC scheme :tangle build/server.scm
  (users (cons* (user-account
                  (name "cylon2p0")
                  (group "users")
                  (password (crypt "linux" "$6$abc"))
                  (supplementary-groups
                    '("wheel" "netdev"
                      "docker" "libvirt" "realtime")))
                %base-user-accounts))

  ;; Add the 'realtime' group
  (groups
   (cons
    (user-group
     (system? #t)
     (name "realtime"))
    %base-groups))
#+END_SRC

** Packages
We save most of the package loading for our independent machine definitions. However, here is a base set of packages for my Server that shouldn't change that often.

#+NAME: packages
#+BEGIN_SRC scheme :tangle build/server.scm
  ;; Install a base set of Server packages
  (packages (append (map specification->package
        '("nss-certs" "vim" "tmux" "gnupg")) %base-packages))

#+END_SRC
** Services
*** Special files

#+NAME: special-files
#+BEGIN_SRC scheme :tangle build/server.scm
  (services
    (append
     (list
     (service special-files-service-type
      `(("/bin/sh" ,(file-append bash "/bin/bash"))
        ("/bin/bash" ,(file-append bash "/bin/bash"))
        ("/usr/bin/env" ,(file-append coreutils "/bin/env"))))
#+END_SRC

*** OpenSSH

#+NAME: openssh-service
#+BEGIN_SRC scheme :tangle build/server.scm
     (service openssh-service-type
      (openssh-configuration
       (port-number 69)
       (gateway-ports? #t)
       (password-authentication? #f)
       (authorized-keys
       `(("cylon2p0",%cylon2p0-ssh-key)))))
#+END_SRC

*** Docker and virtualization

#+NAME: virt-services
#+BEGIN_SRC scheme :tangle build/server.scm
     ;; Docker requires elogind
     (service docker-service-type)
     (service elogind-service-type)
     (service libvirt-service-type
      (libvirt-configuration
       (unix-sock-group "libvirt")
        (tls-port "16555")))
     (service virtlog-service-type
      (virtlog-configuration
       (max-clients 1000)))
     (service qemu-binfmt-service-type
      (qemu-binfmt-configuration
       (platforms (lookup-qemu-platforms "arm" "aarch64"))))
#+END_SRC

*** Networking
**** DHCP

#+NAME: dhcp-services
#+BEGIN_SRC scheme :tangle build/server.scm
     (service dhcp-client-service-type)
#+END_SRC

**** NFtables

#+NAME: nftables-services
#+BEGIN_SRC scheme :tangle build/server.scm
     (service nftables-service-type
      (nftables-configuration
       (ruleset
        (local-file "./nftables.conf"))))
#+END_SRC

**** Wireguard

#+NAME: wireguard-services
#+BEGIN_SRC scheme :tangle build/server.scm
     (wire-vpn
      "Zc4oXOUTuJmi4jVoh3DYlQfZZs6J+TfL17z5y53K+TI="
      '("10.100.0.3"))
#+END_SRC

*** ZRAM

#+NAME: zram-services
#+BEGIN_SRC scheme :tangle build/server.scm
(service zram-device-service-type
          (zram-device-configuration
           (size "4G"))))
#+END_SRC

** Server specific configuration

#+NAME: modify-services
#+BEGIN_SRC scheme :tangle build/server.scm
     (modify-services %base-services
      (guix-service-type config =>
       (guix-configuration
        (inherit config)
         (substitute-urls
          (append (list "https://mirror.brielmaier.net")
           %default-substitute-urls))
         (authorized-keys
          (append (list (local-file "./mirror.brielmaier.net.pub"))
           %default-authorized-guix-keys))))))))
#+END_SRC
