#+TITLE: Readme
#+STARTUP: content

#+CAPTION: Infinity Gauntlet
#+NAME: fig:infinitygauntlet
[[./assets/gauntlet.jpg]]

* Endgame

This repository present configuration for [[https://www.gnu.org/software/guix][Guix System]] installation which is deployed to multiple machines. It provides a quick way for me to install Guix System very quickly with all of the settings intact (99% of the time ;-).

Please do NOT promote this repository on any official Guix communication channels, such as their mailing lists or IRC channel, even in response to support requests!  This is to show respect for the Guix project’s [[http://www.gnu.org/distros/free-system-distribution-guidelines.html][strict policy]] against recommending nonfree software, and to avoid any unnecessary hostility.

** Why Guix System?

Guix has a lot of good stuff a power user may look for. Here are the following features I find very attractive:

- Declarative configurations which enables easier reproducibility. The community behind Guix also happens to focus for reproducible and bootstrappable builds which is interesting for me, at least.

- Offers a transparent binary/source installation process. If a package is not available as a binary, it will just build the package for you instead. The best thing about it, it’s optional with just a simple toggle option =(guix package -i $PACKAGE --fallback)=.

- Straightforward packaging process in case you didn’t find the package you’re looking for. This one is a must (in my opinion) if you want to make an independent Linux distro from scratch. You can either contribute to the official package archive or create one yourself.

- Available options include rollback, letting you switch to previous configurations. This is useful for emergency situations where your current configuration doesn’t work and rework your config. Theoretically, you would have a difficult time screwing your system up. [3] You could also go all the way back to your first installation provided you didn’t garbage-collected your system yet.

- It’s a strictly free distro with the ability to add your own proprietary stuff. As far as I know, there’s no mechanism in Guix that prevents you from adding them. That alone makes it a true free distro for me: the freedom to add your own garbage.

** Getting started

Before you can get this setup, there are some prerequisites to do:
- You’ve decided for a [[https://guix.gnu.org/manual/en/html_node/Manual-Installation.html][manual installation]] and already in a shell.
- The partitions are already setup and mounted in /mnt/ as the root.
- Copied this repository (git, ssh, usb drive, etc.) to the target machine
- Choosing the machine from the section below and running the command from the code block

There is an [[install-guix-system.sh][install-guix-system.sh]] script in this repository which setups
blank encrypted BTRFS partition with subvolumes system on EFI systems in order to
aid with the installation process.

*** Configuration

Generate Guix configuration files for the current host:

#+NAME: system-build
#+BEGIN_SRC shell
$ make
#+END_SRC

Generate Guix configuration files for different host:

#+NAME: system-build
#+BEGIN_SRC shell
$ make HOSTNAME=<HOSTNAME>
#+END_SRC

**** Deployment

Build and deploy Guix System & Home configuration:

#+NAME: system-build
#+BEGIN_SRC shell
$ make install
#+END_SRC

*** Machines

Pick the machine, build the system image.

**** [[Laptop-Rocinante.org][Rocinante]]
#+CAPTION: Lenovo Thinkpad x230
#+NAME:   fig:thinkpadx230
[[./assets/lenovo-thinkpad-x230.jpg]]

#+begin_src shell
make HOSTNAME=rocinante
#+end_src

**** [[Laptop-Zen.org][Zen]]
#+CAPTION: Asus Zenbook x303
#+NAME:   fig:zenbookx303
[[./assets/asus-zenbook-x303.jpg]]

#+begin_src shell
make HOSTNAME=zen
#+end_src

**** [[Laptop-Asgard.org][Asgard]]
#+CAPTION: Dell Precision 5550
#+NAME:   fig:precision5550
[[./assets/dell-precision-5550.jpg]]

#+begin_src shell
make HOSTNAME=asgard
#+end_src

**** [[Server.scm][Werewolf]]
#+CAPTION: Dell Precision T5550
#+NAME:   fig:precisionT5550
[[./assets/dell-precision-t5500.jpg]]

#+begin_src shell
make HOSTNAME=werewolf
#+end_src


* Showcase
#+CAPTION: Desktop showcase
#+NAME: fig:desktopshowcase
[[./assets/desktop.jpg]]
